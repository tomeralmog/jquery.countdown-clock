/*
	https://bitbucket.org/tomeralmog/jquery.countdown-clock/
	Autor:Tomer Almog https://bitbucket.org/tomeralmog/
	License: MIT
*/
(function ( $ ) {
	$.fn.countdownClock = function(options) {
		 var settings = $.extend({
		// These are the defaults.
			width:720,
			bgColor:'#000000',
			textColor:'#FFFFFF',
			clockColor:'#107c10',
			circleLabels :['days','hours','minutes','seconds'],
			countDownDate: [2014, 5, 6, 0],
			labelFontSize:27,
			circleFontSize:22,
			timezoneOffset:0,
			columns:4
		}, options );
		
		var defaults={
			minWidth:320,
			countDownDate: [2014, 5, 6, 0],
			
				
		}
		
		
		
		if(settings.countDownDate.length !== 4){
			settings.countDownDate = defaults.countDownDate
		}
		
		if (settings.width<defaults.minWidth){
			settings.width=defaults.minWidth;
		}
		var canvasSize = parseInt(settings.width / settings.columns)-40;
		 return this.each(function() {
			//create the DOM clock elements
			var circleLabels =settings.circleLabels;
			$(this).append('<div id="timer-clock"></div>');
			for(var i=0; i< circleLabels.length; i++){						
				$('#timer-clock').append('<div class="clock-circle" id="clock-cricle'+i+'"></div>');
				$('#clock-cricle'+i).html(circleLabels[i].toUpperCase()+'<br>');
				$('#clock-cricle'+i).append('<canvas id="'+circleLabels[i]+'" width="'+canvasSize+'" height="'+canvasSize+'"></canvas>');
				$('#clock-cricle'+i).append('<div class="clock-label" id="'+circleLabels[i]+'-label"></div>');
			}
	
			//$('#clock-cricle'+(circleLabels.length-1)).append('<div class="clearfix"><br>&nbsp;</div>');
			var bgcolor_param = settings.bgColor;
		
		
			
			
			
			
			$('#timer-clock').css('width', settings.width+'px');
			$('.clock-circle').css('width', parseInt(settings.width/settings.columns) +'px');									
			$('.clock-circle').css('fontSize', settings.circleFontSize+'px');						
			$('.clock-circle > .clock-label').css('fontSize', settings.labelFontSize+'px');			
			$('.clock-circle > .clock-label').css('width', parseInt(settings.width/settings.columns)+'px');
							
			$('.clock-circle > .clock-label').css('bottom', parseInt(canvasSize/2 - settings.circleFontSize/2)+'px');
			
											
			// create Date object for current location
			var d = new Date(settings.countDownDate[0], settings.countDownDate[1], settings.countDownDate[2], settings.countDownDate[3]);
			
			// convert to msec since Jan 1 1970
			var launchTime = d.getTime();
			
			// obtain local UTC offset and convert to msec
			var localOffset = (d.getTimezoneOffset()) * 60000;
			var timeOffset = settings.timezoneOffset*60*60000;//5 hours
						
			// obtain UTC time in msec
			var launchDate = launchTime - localOffset + timeOffset;
			
			//init charts
			
			var seconds = $("#seconds").get(0).getContext("2d");
			secondsChart = new Chart(seconds);
			
			
			var minutes = $("#minutes").get(0).getContext("2d");
			minutesChart = new Chart(minutes);
			
			var hours = $("#hours").get(0).getContext("2d");
			hoursChart = new Chart(hours);
			
			var days = $("#days").get(0).getContext("2d");
			daysChart = new Chart(days);
			
			
			var domCache={
				secondLabel:$('#seconds-label'),
				minutesLabel:$('#minutes-label'),
				hoursLabel:$('#hours-label'),
				daysLabel:$('#days-label'),
			}
			
			var curMinute = -1;
			var curHour = -1;
			var curDay = -1;
			
			countdown(
			launchDate,
			function(ts) {
			
			  var timespanStr = JSON.stringify(ts, null, '  ');
			  var timespan = JSON.parse(timespanStr);				  
			  updateSeconds(timespan.seconds);
			  updateMinutes(timespan.minutes);
			  updateHours(timespan.hours);
			  updateDays(timespan.days);
			  
			},
			countdown.DAYS|countdown.HOURS|countdown.MINUTES|countdown.SECONDS);
			
		
			
			function updateSeconds(val){
				//no  need to check -cahgnes every time
				
				var completeCircle = 60;
				var data = [
					{
						value: val,
						color:settings.clockColor
					},
					{
						value: (completeCircle-val),
						color:settings.bgColor
					}				
				]
				
				secondsChart.Doughnut(data,{
					segmentShowStroke : false,
					animateRotate : false,
					percentageInnerCutout : 80,
				});	
				
				domCache.secondLabel.html(val);
			}//update seconds
			
			
			
			
			function updateMinutes(val){
				if(curMinute==val) return;
				curMinute=val;
				
				var completeCircle = 60;
				var data = [
					{
						value: val,
						color:settings.clockColor
					},
					{
						value: (completeCircle-val),
						color:settings.bgColor
					}				
				]
				
				minutesChart.Doughnut(data,{
					segmentShowStroke : false,
					animateRotate : false,
					percentageInnerCutout : 80,
				});	
				
				domCache.minutesLabel.html(val);
			}//update minutes
			
			
			
			
			function updateHours(val){
				if(curHour==val) return;
				curHour=val;
				var completeCircle = 24;
				var data = [
					{
						value: val,
						color:settings.clockColor
					},
					{
						value: (completeCircle-val),
						color:settings.bgColor
					}				
				]
				
				hoursChart.Doughnut(data,{
					segmentShowStroke : false,
					animateRotate : false,
					percentageInnerCutout : 80,
				});	
				
				domCache.hoursLabel.html(val);
			}//update Hour
			
			
			function updateDays(val){
				if(curDay==val) return;
				
				curDay=val;
				var completeCircle = 30;
				var data = [
					{
						value: val,
						color:settings.clockColor
					},
					{
						value: (completeCircle-val),
						color:settings.bgColor
					}				
				]
				
				daysChart.Doughnut(data,{
					segmentShowStroke : false,
					animateRotate : false,
					percentageInnerCutout : 80,
				});	
				
				domCache.daysLabel.html(val);
				//createTweetMsg(curDay);	
				
			}//update days
			
		
		
		});
	};
}( jQuery ));


