# [Jquery.Countdown-Clock](https://bitbucket.org/tomeralmog/jquery.countdown-clock/)

Countdown clock for XBOX

##Requires

* JQuery
* [ChartJS](https://github.com/nnnick/Chart.js)
* [Countdownjs](http://countdownjs.org)

##Installation
Include script after the jQuery library (unless you are packaging scripts somehow else):

	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/0.2.0/Chart.min.js"></script>
	<script src="/path/to/countdown.min.js"></script>
    <script src="/path/to/jquery.jquery.countdown-clock.min.js"></script>


##Usage
HTML MARKDOWN:

    <div id="clock-wrapper"></div>
JQUERY

    $("#clock-wrapper").countdownClock(options);


Options:

- inputID:       
	- default:'#tweet-text'    
	- description: textarea element ID
- width:
	- default:720    
	- description:the width of the circle

- bgColor:
	- default:'#000000'    
	- description:the background color of the document
- textColor:
	- default: '#FFFFFF'
	- description:text color,
- clockColor:
	- default: '#107c10'
	- description:color of the circles - xbox default green
- circleLabels:	
	- default: ['days','hours','minutes','seconds'],   
	- description:text and css names
- countDownDate: 
	- default: [2014, 5, 6, 0],   
	- description: the date to count down from
- labelFontSize:
	- default: 27   
	- description:the label font size
- circleFontSize:
	- default: 22
	- description: text in the circle font size
- timezoneOffset:
	- default:0    
	- description:timezone offset- the difference in hours and minutes from Coordinated Universal Time (UTC),
- columns:
	- default: 4
	- description: number of columns (2 will create 2 rows of 2 circles)

###Example of usage:


    $("selector").countdownClock({
 
		width:720,
		bgColor:"#000000",
		textColor:"#FFFFFF",
		clockColor:"#107c10",
		circleLabels :["days","hours","minutes","seconds"],
		countDownDate: [2014, 5, 6, 0],
		labelFontSize:27,
		circleFontSize:22,
		timezoneOffset:0,
		columns:4
	});


## Author


[Tomer Almog](https://bitbucket.org/tomeralmog)

## License
[MIT](http://en.wikipedia.org/wiki/MIT_License)